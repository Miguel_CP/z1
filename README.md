# Z1
Proyecto para prueba de conocimiento, tecnologías usadas: Django - PostgreSQL - GraphQL

## Introducción

Backend para una aplicación que permita registrar Usuarios, que estos creen Ideas y puedan seguirse entre ellos.
Teniendo en cuenta las siguientes historias de usuario (marcadas las desarrolladas):

- [x] Un usuario puede registrarse introduciendo su email y eligiendo un nombre de
usuario libre y una contraseña

- [ ] Un usuario debe ser capaz de logarse utilizando email y contraseña.

- [x] Un usuario debe poder cambiar su contraseña.


- [x] Un usuario debe poder restaurar su contraseña recibiendo un email con un magic
link.

- [x] Un usuario puede publicar una idea como un texto corto en cualquier momento.

- [x] Un usuario puede establecer la visibilidad de una idea: publica (todos pueden
verla), protegida (solo otros usuarios que siguen al usuario de la idea pueden
verla) y privada (solo el usuario que creó la idea puede verla)

- [x] Un usuario puede establecer la visibilidad de una idea en el momento de su
creacion o editarla posteriormente.

- [x] Un usuario puede consultar todas las ideas que ha publicado ordenadas de mas
recientes a mas antiguas.

- [x] Un usuario puede borrar una idea publicada.

- [x] Un usuario puede solicitar seguir a otro usuario

- [x] Un usuario puede ver el listado de solicitudes de seguimiento recibidas y
aprovarlas o denegarlas

- [ ] Un usuario puede ver el listado de gente a la que sigue

- [ ] Un usuario puede ver el listado de gente que le sigue

- [ ] Un usuario puede dejar de seguir a alguien

- [ ] Un usuario puede eliminar a otro usuario de su lista de seguidores

- [x] Un usuario puede realizar una búsqueda de otros usuarios introduciendo un
nombre de usuario o parte de uno

- [ ] Un usuario puede ver la lista de ideas de cualquier otro usuario, teniendo en
cuenta la visibilidad de cada idea.

- [x] Un usuario puede ver un timeline de ideas compuesto por sus propias ideas y las
ideas de los usuarios a los que sigue, teniendo en cuenta la visibilidad de cada
idea.

- [ ] Un usuario debe recibir una notificación cada vez que un usuario al que sigue
publica una idea nueva a la que tiene acceso.

## Requisitos
docker instalado, si tienes Ubuntu puedes seguir esta guía de la pág oficial: https://docs.docker.com/engine/install/ubuntu/

Yo he usado la versión:
```
Docker version 20.10.12, build e91ed57
```
docker-compose instalado, si tienes Ubuntu puedes seguir esta guía de la pág oficial: https://docs.docker.com/compose/install/
 
Puertos libres en el equipo:
 - 8000
 - 5432
 
## Puesta en marcha
Descargar el proyecto e ir a la ruta principal (carpeta 'z1').

Ejecutar docker-compose:
```
sudo docker-compose up 
```

## Panel Admin Django
Crearemos un usuario admin para acceder al panel Admin que incorpora Django en el que podremos ver los modelos creados,
necesitamos ver el ID del contenedor para conectarnos a él:
```
sudo docker ps
```
Nos fijamos en el ID del contenedor llamado 'z1_python' y ejecutamos:
```
sudo docker exec -it <ID del contenedor aquí> bash
```
Ahora podremos ejecutar la siguiente instrucción de Django para crear un usuario administrador:
```
python manage.py createsuperuser
```
Nos preguntará por un nombre, email y contraseña. Lo importante es el nombre y contraseña para poder acceder al panel Admin.

Una vez creado podremos acceder desde la ruta http://127.0.0.1:8000/admin/

Esto solo será necesario la primera vez que levantemos el docker-compose, sino lo borramos ya seguirá el usuario creado.

## Tests
Para ejecutar los tests nos queadmos dentro del contenedor 'z1_python' y ejecutamos:
```
python manage.py test
```

## API

Se puede consultar las peticiones disponibles una vez esté el proyecto en ejecución en la ruta http://127.0.0.1:8000/graphql/. Fijarse en la zona superior derecha.

## Notas
No tenía experiencia ni en GraphQL ni en PostgreSQL por lo que mucho tiempo de la prueba me ha llevado con aprender lo básico, también he tenido muchos problemas en conectar con PostgreSQL debido a la forma de conexión el local por defecto ('peer'). Me ha venido bien para fameliarizarme con estas tecnologías aunque me ha robado tiempo útil para la prueba ya que solo tenía el finde disponible.

Sistema de Login sin implementar, podría haber usado el propio de Django o partir de él.

Registro del campo contraseña inseguro, hay que aplicar un cifrado no reversible (hash) con algoritmo seguro (incluyendo 'salt').

Envío de Email, función creada pero sin configurar un servidor smtp en los settings, por lo que falla el envío, se ha controlado la excepción y se actualiza la constraseña del usuario aunque no debería si el envío falla.

Consulta de Ideas por ID de usuario: están ordenadas por fecha de creación, mostrando las más recientes antes, aunque se puede dar el caso de que haya Ideas editadas más recientes, solo se tendrá en cuenta la fecha de creación.

Test: solo hay 1 por Query y Mutation del modelo User, sería hacer más similares del resto de Query y Mutation pero de otros modelos.

Validaciones de datos: viendo como me ha quedado el código huele un poco y se podrá mejorar, seguro que existe alguna forma de abstraer las validaciones del proceso de mutation y generar un código más legible. Me ha faltado tiempo para investigarlo.

Notificación (sin hacer): Este tema depende de la conexión del front, he visto que GraphQL tiene subscriptions por lo que posiblemente sea ese el camino. Si fuera por websocket se podría implementar con ayuda de django-channel o cualquier librería de emisor/subscriptor.
Lo haría a la hora de crear una Idea, revisaría los seguidores en la tabla Relationship filtrando por el ID del usuario que crea la Idea en la columna to_user y que el seguimiento este confirmado:
```
Relationship.objects.filter(to_user=input.user_id, active=True)
```
De esa forma tendría los IDs de los seguidores y mandaría la notificación a cada uno de ellos.

# Importante
Este proyecto está en desarrollo y pensado para ejecución local, no tiene settings.py para ejecutar en producción de forma segura.
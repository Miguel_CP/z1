FROM python:3.10

ENV PYTHONUNBUFFERED 1

COPY ./app/ /home/usuario/app/
COPY ./Pipfile /home/usuario/app/
COPY ./Pipfile.lock /home/usuario/app/

WORKDIR /home/usuario/app/

RUN pip install pipenv && pipenv install --system

EXPOSE 8000

CMD [ "python3", "manage.py", "runserver"]

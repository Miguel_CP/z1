from django.contrib import admin

from api.forms import UserForm
from api.models import User, Idea, Relationship


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'email', 'password', 'created_at', 'updated_at',)
    form = UserForm


@admin.register(Idea)
class IdeaAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'content', 'visibility', 'created_at', 'updated_at')
    list_filter = ('visibility',)


@admin.register(Relationship)
class RelationshipAdmin(admin.ModelAdmin):
    list_display = ('id', 'from_user', 'to_user', 'active', 'created_at', 'updated_at',)

import json

from graphene_django.utils.testing import GraphQLTestCase

from api.models import User


class TestUsersQuery(GraphQLTestCase):
    def setUp(self):
        self.user1 = User.objects.create(name="user1", email="email1")
        self.user2 = User.objects.create(name="miguel", email="email2")

    def test_all_users_query(self):
        response = self.query(
            '''
            query {
                allUsers {
                    id
                    name
                }
            }
            '''
        )
        content = json.loads(response.content)
        self.assertResponseNoErrors(response)
        self.assertEqual(2, len(content['data']['allUsers']))
        self.assertEqual(str(self.user1.pk), content['data']['allUsers'][0]['id'])
        self.assertEqual(self.user1.name, content['data']['allUsers'][0]['name'])

    def test_users_by_name_query(self):
        response = self.query(
            '''
            query {
              userByName(name:"mi"){
                id
                name
                email
              }
            }
            '''
        )
        content = json.loads(response.content)
        self.assertResponseNoErrors(response)
        self.assertEqual(1, len(content['data']['userByName']))
        self.assertEqual(str(self.user2.pk), content['data']['userByName'][0]['id'])
        self.assertEqual(self.user2.name, content['data']['userByName'][0]['name'])
        self.assertEqual(self.user2.email, content['data']['userByName'][0]['email'])

    def test_create_user_mutation(self):
        response = self.query(
            '''
            mutation createUser($input: UserInput!) {
                createUser(input: $input) {
                    user {
                        id
                        name
                        email
                    }
                }
            }
            ''',
            op_name='createUser',
            input_data={'name': 'name_test', 'email': 'email_test', 'password': 'pass_test'}
        )
        content = json.loads(response.content)
        user = User.objects.get(name='name_test')
        self.assertResponseNoErrors(response)
        self.assertEqual(str(user.pk), content['data']['createUser']['user']['id'])


import graphene

from api.graphql import user, idea, relationship


class Query(user.queries.Query, idea.queries.Query, relationship.queries.Query):
    pass


class Mutation(user.mutations.Mutation, idea.mutations.Mutation, relationship.mutations.Mutation):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)

import graphene

from api.graphql.relationship.types import RelationshipType
from api.models import Relationship, User


class RelationshipInput(graphene.InputObjectType):
    from_user = graphene.Int(required=True)
    to_user = graphene.Int(required=True)
    active = graphene.Boolean()


class CreateRelationship(graphene.Mutation):
    class Arguments:
        input = RelationshipInput(required=True)

    ok = graphene.Boolean()
    relationship = graphene.Field(RelationshipType)

    def mutate(root, info, input):
        if input.from_user == input.to_user:
            raise ValueError('No se puede relacionar el mismo Usuario.')
        from_user = User.objects.filter(pk=input.from_user).first()
        if not from_user:
            raise ValueError("Usuario from no existe")
        to_user = User.objects.filter(pk=input.to_user).first()
        if not to_user:
            raise ValueError('Usuario to no existe.')
        try:
            relationship = Relationship.objects.create(from_user_id=input.from_user, to_user_id=input.to_user, active=input.active)
        except:
            raise ValueError('La relación de los dos Usuarios ya existe.')
        return CreateRelationship(ok=True, relationship=relationship,)


class ActivateRelationship(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        user_id = graphene.Int(required=True)

    ok = graphene.Boolean()
    relationship = graphene.Field(RelationshipType)

    def mutate(root, info, id, user_id):
        try:
            relationship = Relationship.objects.get(pk=id, to_user=user_id)
        except Exception:
            raise ValueError('La Relación no existe')
        relationship.active = True
        relationship.save()
        return CreateRelationship(ok=True, relationship=relationship,)


class DeleteRelationship(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        user_id = graphene.Int(required=True)

    ok = graphene.Boolean()
    relationship = graphene.Field(RelationshipType)

    def mutate(root, info, id, user_id):
        try:
            relationship = Relationship.objects.get(pk=id, to_user=user_id)
        except Exception:
            raise ValueError('La Relación no existe')
        relationship.delete()
        return DeleteRelationship(ok=True,)


class Mutation(graphene.ObjectType):
    create_relationship = CreateRelationship.Field()
    activate_relationship = ActivateRelationship.Field()
    delete_relationship = DeleteRelationship.Field()

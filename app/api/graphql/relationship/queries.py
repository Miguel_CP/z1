import graphene

from api.graphql.relationship.types import RelationshipType
from api.models import Relationship


class Query(graphene.ObjectType):
    follow_up_request_by_user_id = graphene.List(RelationshipType, user_id=graphene.Int(required=True))

    def resolve_follow_up_request_by_user_id(root, info, user_id):
        return Relationship.objects.filter(to_user=user_id)

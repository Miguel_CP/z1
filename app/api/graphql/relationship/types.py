from graphene_django import DjangoObjectType

from api.models import Relationship


class RelationshipType(DjangoObjectType):
    class Meta:
        model = Relationship
        fields = ("id", "from_user", "to_user", "active")

import graphene

from api.graphql.idea.types import IdeaType
from api.models import Idea, Relationship


class Query(graphene.ObjectType):
    ideas_by_user_id = graphene.List(IdeaType, user_id=graphene.Int(required=True))
    ideas_own_and_followers = graphene.List(IdeaType, user_id=graphene.Int(required=True))

    def resolve_ideas_by_user_id(root, info, user_id):
        return Idea.objects.filter(user_id=user_id)

    def resolve_ideas_own_and_followers(root, info, user_id):
        own_ideas = Idea.objects.filter(user_id=user_id)
        users = Relationship.objects.filter(to_user=user_id, active=True).values_list('from_user', flat=True)
        follower_ideas = Idea.objects.filter(user_id__in=users, visibility__lte=2)

        return own_ideas.union(follower_ideas).order_by('-created_at')

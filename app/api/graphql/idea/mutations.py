import graphene

from api.graphql.idea.types import IdeaType
from api.models import Idea, User


class IdeaInput(graphene.InputObjectType):
    user_id = graphene.Int(required=True)
    content = graphene.String(required=True)
    visibility = graphene.Int(required=True, description='1-Público | 2-Protegido | 3-Privado')


class CreateIdea(graphene.Mutation):
    class Arguments:
        input = IdeaInput(required=True)

    ok = graphene.Boolean()
    idea = graphene.Field(IdeaType)

    def mutate(root, info, input):
        if not input.content.strip():
            raise ValueError('No se permite crear una Idea sin contenido.')
        if len(input.content) > 140:
            raise ValueError('Contenido demasiado largo, máximo 140 caracteres')
        if input.visibility < 1 or input.visibility > 3:
            raise ValueError('La visibilidad debe ser un número entre 1 y 3.')
        try:
            user = User.objects.get(pk=input.user_id)
        except Exception:
            raise ValueError('El Usuario no existe.')
        idea = Idea(user_id=user.pk, content=input.content, visibility=input.visibility)
        idea.save()
        ok = True
        return CreateIdea(ok=ok, idea=idea,)


class UpdateIdeaVisibility(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        visibility = graphene.Int(required=True)

    ok = graphene.Boolean()
    idea = graphene.Field(IdeaType)

    def mutate(root, info, id, visibility):
        if visibility < 1 or visibility > 3:
            raise ValueError('La visibilidad debe ser un número entre 1 y 3.')
        try:
            idea = Idea.objects.get(pk=id)
        except Exception:
            raise ValueError('La idea no existe.')
        if idea:
            idea.visibility = visibility
            idea.save()
            return UpdateIdeaVisibility(ok=True, idea=idea)
        return UpdateIdeaVisibility(ok=False, idea=None,)


class DeleteIdea(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)

    ok = graphene.Boolean()
    idea = graphene.Field(IdeaType)

    def mutate(root, info, id):
        try:
            idea = Idea.objects.get(pk=id)
        except Exception:
            raise ValueError('La idea no existe.')
        if idea:
            idea.delete()
            return DeleteIdea(ok=True)
        return DeleteIdea(ok=False)


class Mutation(graphene.ObjectType):
    create_idea = CreateIdea.Field()
    update_idea_visibility = UpdateIdeaVisibility.Field()
    delete_idea = DeleteIdea.Field()

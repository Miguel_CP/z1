from graphene_django import DjangoObjectType

from api.models import Idea


class IdeaType(DjangoObjectType):

    class Meta:
        model = Idea
        fields = ("id", "user", "content", "visibility", "created_at", "updated_at")
        convert_choices_to_enum = False

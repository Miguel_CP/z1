import graphene
from graphene import relay

from api.graphql.user.types import UserType, UserConnection
from api.models import User


class Query(graphene.ObjectType):
    all_users = graphene.List(UserType)
    user_by_name = graphene.List(UserType, name=graphene.String(required=True))
    paginated_users = relay.ConnectionField(UserConnection)

    def resolve_all_users(root, info):
        return User.objects.all()

    def resolve_user_by_name(root, info, name):
        return User.objects.filter(name__icontains=name)

    def resolve_paginated_users(root, info, **kwargs):
        return User.objects.all()

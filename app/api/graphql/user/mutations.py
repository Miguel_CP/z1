import random
import string

import graphene

from api.graphql.user.types import UserType
from api.models import User
from api.utils import send_password_email


class UserInput(graphene.InputObjectType):
    name = graphene.String(required=True)
    email = graphene.String(required=True)
    password = graphene.String(required=True)


class CreateUser(graphene.Mutation):
    class Arguments:
        input = UserInput(required=True)

    ok = graphene.Boolean()
    user = graphene.Field(UserType)

    def mutate(root, info, input):
        if not input.name.strip() or not input.email.strip() or not input.password.strip():
            raise ValueError('No se permiten campos vacíos.')
        try:
            user = User.objects.create(name=input.name, email=input.email, password=input.password)
        except Exception as ex:
            field_name = ''
            if 'email' in ex.args[0]:
                field_name = 'email'
            if 'name' in ex.args[0]:
                field_name = 'nombre'
            raise ValueError(f'El {field_name} ya existe.')
        return CreateUser(ok=True, user=user,)


class UpdateUserPassword(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)
        password = graphene.String(required=True)

    ok = graphene.Boolean()
    user = graphene.Field(UserType)

    def mutate(root, info, id, password):
        try:
            user = User.objects.get(pk=id)
        except Exception:
            raise ValueError('El Usuario no existe.')

        if not password.strip():
            raise ValueError('No se permite contraseña vacía.')

        if user:
            user.password = password
            user.save()
            return UpdateUserPassword(ok=True, user=user)
        return UpdateUserPassword(ok=False, user=None)


class GeneratePasswordUser(graphene.Mutation):
    class Arguments:
        id = graphene.Int(required=True)

    ok = graphene.Boolean()
    user = graphene.Field(UserType)

    def mutate(root, info, id):
        try:
            user = User.objects.get(pk=id)
        except Exception:
            raise ValueError('El Usuario no existe.')

        if user:
            letters = string.ascii_lowercase + string.ascii_uppercase + string.digits
            user.password = ''.join(random.choice(letters) for _ in range(18))
            try:
                send_password_email(user)
                user.save()
            except Exception as ex:
                pass
                user.save()  # no debería guardarse si el envío falla. Como no hay servidor smtp configurado se guarda para poder visualizarlo.
            return GeneratePasswordUser(ok=True, user=user)
        return GeneratePasswordUser(ok=False, user=None)


class Mutation(graphene.ObjectType):
    create_user = CreateUser.Field()
    update_user_password = UpdateUserPassword.Field()
    generate_user_password = GeneratePasswordUser.Field()

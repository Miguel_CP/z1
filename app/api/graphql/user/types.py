from graphene import relay
from graphene_django import DjangoObjectType

from api.models import User


class UserType(DjangoObjectType):
    class Meta:
        model = User
        fields = ("id", "name", "email")


class UserConnection(relay.Connection):
    class Meta:
        node = UserType

from django.db import models


class TimeStampMixin(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class User(TimeStampMixin):
    name = models.CharField(max_length=20, unique=True)
    email = models.CharField(max_length=254, unique=True)
    password = models.CharField(max_length=50)

    objects = models.Manager()

    def __str__(self):
        return f'{self.pk} - {self.name}'


class Idea(TimeStampMixin):
    VISIBILITIES = [
        (1, 'Público'),
        (2, 'Protegido'),
        (3, 'Privado'),
    ]

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='ideas')
    content = models.CharField(max_length=140)
    visibility = models.IntegerField(choices=VISIBILITIES)

    objects = models.Manager()

    class Meta:
        ordering = ['-created_at']


class Relationship(TimeStampMixin):
    from_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='source')
    to_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='destination')
    active = models.BooleanField(default=False)

    objects = models.Manager()

    class Meta:
        unique_together = ('from_user', 'to_user',)

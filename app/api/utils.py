from django.conf import settings
from django.core.mail import send_mail


def send_password_email(user):
    send_mail(
        'Z1 - Su contraseña a sido reestablecida',
        f'Su nueva contraseña es: {user.password}',
        settings.EMAIL_HOST_USER,
        [user.email],
        fail_silently=False
    )
